## Repozytorium testowe

Jeśli udało się sklonować to repozytorium i czytasz ten plik na swoim komputerze to wszystko się udało i GIT będzie działał na szkoleniu.

Nie musisz robić nic więcej z GITem. 

## NodeJS i NPM.

Jeśli szkolenie wymaga pracy z NodeJS wykonaj następujące polecenie w katalogu z niniejszym plikiem:

npm install 

Jeśli wszystko poszło dobrze powinien powstać katalog "node_modules" z bibliotekami pobranymi z npm.org